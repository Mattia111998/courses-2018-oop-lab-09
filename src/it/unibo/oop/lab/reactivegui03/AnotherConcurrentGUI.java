package it.unibo.oop.lab.reactivegui03;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GraphicsConfiguration;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import it.unibo.oop.lab.reactivegui02.ConcurrentGUI;

public class AnotherConcurrentGUI extends JFrame {

    private static final long serialVersionUID = 1L;
    private static final double WIDTH_PERC = 0.2;
    private static final double HEIGHT_PERC = 0.1;
    private final JLabel display = new JLabel();
    private final JButton stop = new JButton("stop");
    private final JButton up = new JButton("up");
    private final JButton down = new JButton("down");
    
    public AnotherConcurrentGUI()  {
        super();
        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setSize((int) (screenSize.getWidth() * WIDTH_PERC), (int) (screenSize.getHeight() * HEIGHT_PERC));
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        final JPanel panel = new JPanel();
        panel.add(display, BorderLayout.LINE_START);
        panel.add(down, BorderLayout.CENTER);
        panel.add(stop, BorderLayout.EAST);
        panel.add(up, BorderLayout.WEST);
        this.getContentPane().add(panel);
        this.setVisible(true);
        
        final Agent agent = new Agent();
        final AgentTime agentTime = new AgentTime(agent);
        new Thread(agent).start();
        new Thread(agentTime).start();
        
        
        
        stop.addActionListener(new ActionListener() {
            /**
             * event handler associated to action event on button stop.
             * 
             * @param e
             *            the action event that will be handled by this listener
             */
            @Override
            public void actionPerformed(final ActionEvent e) {
                // Agent should be final
                agent.stopCounting();
                disableAll();
            }
        });
        
        down.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                agent.setFlagCounting(false);
            }
        });
        
        up.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                agent.setFlagCounting(true);
            }
        });
    }
    
    private void disableAll() {
        stop.setEnabled(false);
        up.setEnabled(false);
        down.setEnabled(false);
    }
    
    private class Agent implements Runnable {
        /*
         * stop is volatile to ensure ordered access
         */
        private volatile boolean stop;
        private volatile boolean flag = true;
        private int counter;

        public void run() {
            while (!this.stop) {
                try {
                    /*
                     * All the operations on the GUI must be performed by the
                     * Event-Dispatch Thread (EDT)!
                     */
                    SwingUtilities.invokeAndWait(new Runnable() {
                        public void run() {
                            AnotherConcurrentGUI.this.display.setText(Integer.toString(Agent.this.counter));
                        }
                    });
                    if(getFlagCounting()) { 
                        this.counter++;
                    } else {
                        this.counter--;
                    }
                    Thread.sleep(100);
                } catch (InvocationTargetException | InterruptedException ex) {
                    /*
                     * This is just a stack trace print, in a real program there
                     * should be some logging and decent error reporting
                     */
                    ex.printStackTrace();
                }
            }
        }

        /**
         * External command to stop counting.
         */
        public void stopCounting() {
            this.stop = true;
        }
        
        public void setFlagCounting(final boolean flag) {
            this.flag = flag;
        }
        
        public boolean getFlagCounting() {
            return this.flag;
        }
    }
    
    private class AgentTime implements Runnable {
        private static final int TIME = 10000; //10 seconds
        private volatile long startTime;  
        private final Agent agent;
            
        public AgentTime(final Agent agent) {
            this.agent = agent;
        }
        public void run() {
            startTime = System.currentTimeMillis();
            while (true) {
                try {
                    
                    /*
                     * All the operations on the GUI must be performed by the
                     * Event-Dispatch Thread (EDT)!
                     */
                    SwingUtilities.invokeAndWait(new Runnable() {
                        public void run() {
                            if(System.currentTimeMillis() - startTime == TIME) {
                                agent.stopCounting();
                                AnotherConcurrentGUI.this.disableAll();
                            }
                        }
                    });
                } catch (InvocationTargetException | InterruptedException ex) {
                    /*
                     * This is just a stack trace print, in a real program there
                     * should be some logging and decent error reporting
                     */
                    ex.printStackTrace();
                }
            }
        }
    }

}
