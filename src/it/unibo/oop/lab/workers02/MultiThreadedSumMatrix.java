package it.unibo.oop.lab.workers02;

import java.util.List;

public class MultiThreadedSumMatrix implements SumMatrix {
    
    private final int threads;
    
    public MultiThreadedSumMatrix(final int threads) {
        this.threads = threads;
    }
    
    private static class Worker extends Thread {
        private final double[][] matrix;
        private final int startpos;
        private final int nelem;
        private long res;
        
        
        public Worker(final double[][] matrix, final int startpos, final int nelem) {
            super();
            this.matrix = matrix;
            this.startpos = startpos;
            this.nelem = nelem;
        }
        
        @Override
        public void run() {
            System.out.println("Working from position " + startpos + " to position " + (startpos + nelem - 1));
            for (int i = startpos; i < list.size() && i < startpos + nelem; i++) {
                this.res += this.list.get(i);
            }
        }

        /**
         * Returns the risult of summing up the integers within the list.
         * 
         * @return the sum of every element in the array
         */
        public long getResult() {
            return this.res;
        }
    }

    @Override
    public double sum(double[][] matrix) {
        // TODO Auto-generated method stub
        return 0;
    }

}
